#!/bin/bash

D() {
    test -z $KYO_DEBUG || echo $1
}

# docker安装及配置
docker_install() {
    test -d /etc/docker || mkdir -p /etc/docker
    grep -q 'a5g1zyh1' /etc/docker/daemon.json || cat > /etc/docker/daemon.json << EOF
{
    "registry-mirrors": ["https://a5g1zyh1.mirror.aliyuncs.com"]
}
EOF
    which docker &> /dev/null && return 0

    local name=docker-ce-20.10.2-3.el7.x86_64.rpm

    cd ~
    curl -o $name http://mirrors.aliyun.com/docker-ce/linux/centos/7/x86_64/stable/Packages/$name || return 1

    yum install $name -y
    rm $name -f

    systemctl enable docker.service
    systemctl start docker.service

    cd $rootPath
}

# 关闭防火墙
close_firewall() {
	# 停止firewall
	systemctl stop firewalld.service
	# 禁止firewall开机启动
	systemctl disable firewalld.service
}

# 关闭selinux
close_selinux() {
	sed -i 's/^.*SELINUX=[a-zA-Z].*$/SELINUX=disabled/' /etc/selinux/config
}

# 必备软件安装
software_install() {
	# 阿里云源
    local repofile=/etc/yum.repos.d/CentOS-Base.repo
    grep -q 'mirrors.aliyun.com' $repofile \
        || curl -o $repofile http://mirrors.aliyun.com/repo/Centos-7.repo

    local i=1
    which tmux &> /dev/null || i=0
    which git &> /dev/null || i=0
    which vim &> /dev/null || i=0
    which sudo &> /dev/null || i=0
    which ifconfig &> /dev/null || i=0

    test "$i" -eq 1 && return 0

    # 检测网络
    ping www.baidu.com -c 2 &> /dev/null || return 1

	# 安装基本软件
	yum makecache
	yum install -y tmux git vim sudo net-tools
}

# ssh密钥配置
ssh_config() {
	test -e ~/.ssh || mkdir ~/.ssh
    cp $rootPath/node.pub ~/.ssh/authorized_keys
}

hostname_config() {
	test -z $1 && return 1
	# 主机名修改
	hostnamectl set-hostname $1

	# 修改hosts
	local hosts=/etc/hosts
	grep -q "$1" $hosts || echo "127.0.0.1 $1" >> $hosts
}

setIp() {
	local ip_num=$1
    grep -q '\.' <<< "$ip_num" || ip_num=${ip_net}.${ip_num}
	local eth_name=$(ifconfig | egrep "^e" | awk -F ':' '{print $1}' | head -n 1)
	cat > /etc/sysconfig/network-scripts/ifcfg-$eth_name << EOF
TYPE="Ethernet"
BOOTPROTO="static"
NAME="$eth_name"
DEVICE="$eth_name"
ONBOOT="yes"
IPADDR="${ip_num}"
NETMASK="255.255.255.0"
GATEWAY="${ip_net}.1"
DNS1="${ip_net}.1"
DNS2="8.8.8.8"
EOF
}

grub_config() {
    local grubfile=/boot/grub2/grub.cfg
    tail -n 1 $grubfile | grep -q 'set timeout' \
        || echo 'set timeout=0' >> /boot/grub2/grub.cfg
}

sys_config() {
    D 'common config...'
    # 执行通用配置
    source common.sh $git_user $git_email

    D 'DNS config...'
    cat > /etc/resolv.conf << EOF
nameserver 223.5.5.5
nameserver 8.8.8.8
EOF
    D 'close selinux...'
    close_selinux

    D 'close firewall...'
    close_firewall

    D 'ssh config...'
    ssh_config

    D 'set hostname...'
    hostname_config $host_name

    D 'setting ip address...'
    # 设置固定IP地址
    setIp $ip

    D 'grub2 menu set timeout....'
    # 设置grub2菜单时间
    grub_config

    D 'software_install...'
    software_install

    D 'docker config...'
    docker_install
}

parse_args() {
    GETOPT_ARGS=$(getopt -o "m:h:i:u:e:t:n:d:s:r" -- "$@")
    eval set -- "$GETOPT_ARGS"

    while test -n "$1" ; do
        case "$1" in
            -m) export PS1_NAME=$2; shift 2;;
            -h) host_name=$2; shift 2;;
            -i) ip=$2; shift 2;;
            -u) git_user=$2; shift 2;;
            -e) git_email=$2; shift 2;;
            -t) export KYO_TMUX=$2; shift 2;;
            -n) ip_net=$2; shift 2;;
            -r) is_reboot=1; shift 1;;
            --) shift 1; break ;;
            *) break ;;
        esac
    done
    args="$@"
}

ip_net=192.168.88
host_name=node
ip=50
git_user=kyo
git_email=iprintf@qq.com
export rootPath=/config
# centos的tmux版本比较旧
export KYO_TMUX=1
export KYO_SERVER_VIM=1

# 解析参数
parse_args "$@"

# 切换工作路径
cd $rootPath

sys_config

# 配置完成后是否重启
test -z "$is_reboot" || reboot

echo -e '\033[34;1mConfig Finish!!!!\033[0m'

