# interfaces(5) file used by ifup(8) and ifdown(8)
auto lo
iface lo inet loopback

auto KYO_DEV
# iface KYO_DEV inet dhcp
iface KYO_DEV inet static
address 3.3.3.3
netmask 255.255.254.0
gateway 3.3.3.1

auto KYO_DEV:0
iface KYO_DEV inet static
address 192.168.4.186
netmask 255.255.255.0
gateway 192.168.4.1

auto KYO_DEV:1
iface KYO_DEV inet static
address 192.168.2.20
netmask 255.255.254.0
gateway 192.168.2.1

auto KYO_DEV:2
iface KYO_DEV inet static
address 192.168.0.253
netmask 255.255.255.0
gateway 192.168.0.1

auto KYO_DEV:3
iface KYO_DEV inet static
address 192.168.1.253
netmask 255.255.255.0
gateway 192.168.1.1
