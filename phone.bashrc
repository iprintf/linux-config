ps1_name=${PS1_NAME-kyo@phone.redmi}

#PS1 terminal title
if [ $UID -eq 0 ]
then
    export PS1="\[\033[01;31m\]${ps1_name}\[\033[01;35m\] \w\n\[\033[00m\]# "
else
    export PS1="\[\033[01;31m\]${ps1_name}\[\033[01;35m\] \w\n\[\033[00m\]$ "
fi

#terminal vim
set -o vi

#tmux
alias ktmux='tmux -2 new -s kyo'
alias tmuxlist='tmux list-session'

#TERM (ls /lib/terminfo/x/)
export TERM=xterm-256color

export PATH=$PATH:~/linux-config/bin

# 中文支持
# export LANG=C.UTF-8
# export LANGUAGE=C.UTF-8

export LD_LIBRARY_PATH=$PREFIX/lib/openssl-1.1

