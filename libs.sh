#!/bin/bash

err_exit() {
    echo $1
    exit $2
}

# 符号安装函数
sym_install() {
    local src="$1"
    local dst="$2"
    test -z "$src" && return 1
    test -z "$dst" && return 1
    test -L "$dst" && return 1
    rm "$dst" -rf &> /dev/null
    ln -s "$src" "$dst"
}

# 追加安装函数
append_install() {
    local src="$1"
    local dst="$2"
    test -z "$src" && return 1
    test -z "$dst" && return 1
    grep ". $src" "$dst" &> /dev/null     \
                || echo ". $src" >> "$dst"
}

# 覆盖安装函数
cover_install() {
    local src="$1"
    local dst="$2"
    test -z "$src" && return 1
    test -z "$dst" && return 1
    rm "$dst" -rf &> /dev/null
    /bin/cp "$src" "$dst" -rf
}

