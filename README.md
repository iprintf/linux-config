# kyo-linux-config

公共配置

    bash配置
        命令提示符名称可指定
    vim配置
    tmux配置
        不同版本指定
    git配置
        git用户标识指定

个人电脑(公共配置)

    加密程序
    GPG密钥
    ssh密钥及配置
    shadow-git
    log-with-git(kdy)
    桌面快捷键
        org.cinnamon.desktop.keybindings.wm increase-opacity ['<Primary><Alt>equal']
    解锁加密磁盘工具

centos7服务器(公共配置)

    阿里云源
        curl -o /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo
    关闭selinux
        修改/etc/selinux/config文件SELINUX字段为disabled
    关闭防火墙
        systemctl stop firewalld.service #停止firewall
        systemctl disable firewalld.service #禁止firewall开机启动
    设置固定IP地址
    中文配置
    修改hosts
    主机名修改
    ssh服务开启
    docker安装及配置
    grub菜单时间调整

ubuntu服务器(公共配置)

    阿里云源
    设置固定IP地址
    中文配置
    修改hosts
    主机名修改
    ssh服务开启
    docker安装及配置

-------------------------------------------------------------------------------

