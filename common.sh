#!/bin/bash

source libs.sh

test -z "$rootPath" && rootPath=$(pwd)
test -z "$KYO_SERVER_VIM" && vimname=vim || vimname=server-vim
vimPath="$rootPath/$vimname"

vim_config() {
    cd "$rootPath"
    test -e "$vimPath" || git clone https://gitee.com/iprintf/$vimname
    cd $vimPath
    ./linux_install.sh &> /dev/null
    cd ~
    rm *.tar -f
    cd $rootPath
}

test -z "$PS1_NAME" \
    || sed -i "s/^ps1_name=.*$/ps1_name=$PS1_NAME/" $rootPath/bashrc

append_install $rootPath/bashrc ~/.bashrc
sym_install $rootPath/inputrc ~/.inputrc
sym_install $rootPath/tmux${KYO_TMUX-2}.conf ~/.tmux.conf
sym_install $rootPath/gitconfig ~/.gitconfig
vim_config

name="${1-kyo}"
email="${2-iprintf@qq.com}"

test -z "$name" || git config --global user.name $name
test -z "$email" || git config --global user.email $email

